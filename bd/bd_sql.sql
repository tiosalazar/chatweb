-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema chat_web
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema chat_web
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `chat_web` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `chat_web` ;

-- -----------------------------------------------------
-- Table `chat_web`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `chat_web`.`user` (
  `iduser` INT NOT NULL COMMENT '',
  `nombre` VARCHAR(255) NOT NULL COMMENT '',
  `password` VARCHAR(255) NOT NULL COMMENT '',
  PRIMARY KEY (`iduser`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chat_web`.`sala`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `chat_web`.`sala` (
  `idsala` INT NOT NULL COMMENT '',
  `user_iduser` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idsala`, `user_iduser`)  COMMENT '',
  INDEX `fk_sala_user_idx` (`user_iduser` ASC)  COMMENT '',
  CONSTRAINT `fk_sala_user`
    FOREIGN KEY (`user_iduser`)
    REFERENCES `chat_web`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `chat_web`.`mensaje`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `chat_web`.`mensaje` (
  `idmensaje` INT NOT NULL COMMENT '',
  `hora` DATETIME(6) NOT NULL COMMENT '',
  `user_iduser` INT NOT NULL COMMENT '',
  `sala_idsala` INT NOT NULL COMMENT '',
  `sala_user_iduser` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idmensaje`, `user_iduser`, `sala_idsala`, `sala_user_iduser`)  COMMENT '',
  INDEX `fk_mensaje_user1_idx` (`user_iduser` ASC)  COMMENT '',
  INDEX `fk_mensaje_sala1_idx` (`sala_idsala` ASC, `sala_user_iduser` ASC)  COMMENT '',
  CONSTRAINT `fk_mensaje_user1`
    FOREIGN KEY (`user_iduser`)
    REFERENCES `chat_web`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mensaje_sala1`
    FOREIGN KEY (`sala_idsala` , `sala_user_iduser`)
    REFERENCES `chat_web`.`sala` (`idsala` , `user_iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
