<?php
/*
*	ARCHIVO DE CONFIGURACION CON  LAS VARIABLES A UTILIZAR EN LA APLICACION
*/
    //si se pasa el id se la sesion como parametro trata de restaurar la sesion
	session_name("ChatWeb");
	session_start();
	 
	ini_set("memory_limit", "128000000");
	ini_set("mssql.charset", "ISO-8859-1");
  date_default_timezone_set('America/Bogota');

	$_limit = 20;
	$_hora_limite_cancelacion = 4;

	$_host_db = "localhost";
	$_db = "chat_web";
	$_user_db="root";
	$_password_db = "";
 
	$_PATH_WEB = "http://localhost/ChatWeb/";
	$_PATH_SERVIDOR = "http://localhost/ChatWeb/";
	$_PATH_IMAGENES ="http://localhost/ChatWeb/img/";


	$datos = array_merge($_GET,$_POST);
?>
